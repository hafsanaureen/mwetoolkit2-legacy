#! /usr/bin/env python 
# -*- coding: utf-8 -*-

################################################################################
#
# Copyright 2010-2014 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
#
# annotate_crf.py is part of mwetoolkit
#
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################

#CRFsuite license
################################################################################

# Copyright (c) 2002-2016, Naoaki Okazaki 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials# provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################
"""
   Prints a list of predict tags, given a file in dimsum format, using a model learn with train_crf.py
   Use the script dimsumToCRF to transform in a valid format for crfsuite. 
"""


from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import re
import sys
import os
import fileinput
import dimsumToCRF

from libs.base.word import Word
from libs import util
from libs import filetype

usage_string = """\
Usage: {progname} OPTIONS <corpus>
Convert input corpus to lower case.

The <corpus> input file must be in one of the filetype
formats accepted by the `--from` switch.


OPTIONS may be:

--from <input-filetype-ext>
    Force conversion from given filetype extension.
    (By default, file type is automatically detected):
    {descriptions.input[corpus]}

--to <output-filetype-ext>
    Convert input to given filetype extension.
    (By default, keeps input in original format):
    {descriptions.output[corpus]}

-f or --pathListFeatures <listFeatures.txt>
    To give a file containing a features list
    (must have the same format you can see in mwetoolkit/test/CRF/listFeatures.txt,
     which is the list uses by default)

-a or --pathFileAM <AM.quantise>
    To give a file containing association measures
    (you can find an example in mwetoolkit/test/CRF/result_AM_EN.quantise)

-l or --pathFileLF <LF.txt>
      WARNING: not implemented! Only mentioned for documentation
    To give a file containing lexical features

-e or --eval
    If your file contains gold BIO, and you want to have precision, recall and F1 mesure

-c or --crfsuite <crfsuite>
    path to the executable version of crfsuite,
    if you don't want to use default version

-m or --model <CRF.model>
    path where the file containing a model is store.
    By default, try to find a model in test/CRF/CRF.model

-k or --keepCRFfile
    If you decide to keep the intermediary file, containing CRFsuite format.
    This file will be keep in bin/ repository  (in CRFfile.CRF).


{common_options}
"""

#################
# Main script


#Use to avoid encoding problems
reload(sys)
sys.setdefaultencoding('utf8')

args = util.read_options(dimsumToCRF.shortopts, dimsumToCRF.longopts, dimsumToCRF.treat_options, -1, usage_string )
if args == []:
    sys.stderr.write("You forget to give dimsum file!\n")
    exit(1)

fileCRF = open(dimsumToCRF.BIN + "CRFfile.crf", "w") #Create or ecrase the intermediary file where we write in CRFsuite format
fileCRF.close()

util.verbose("Transform in CRFsuite format...\n\n")
filetype.parse(args, dimsumToCRF.DimsumToCRF(), dimsumToCRF.input_filetype_ext)

if not os.path.exists(dimsumToCRF.model): #Verify the path to the model
    sys.stderr.write("Error : "+dimsumToCRF.model + " not found\n")
    exit(1)
import subprocess
util.verbose("Tagging!\n\n")

cmd = "./"+dimsumToCRF.pathCRFsuite+" tag -m "+dimsumToCRF.model+" "+dimsumToCRF.BIN +"CRFfile.crf"
print(subprocess.check_output(['bash','-c', cmd])) #launch the tagging
if dimsumToCRF.eval : #if --eval, try to evaluate recall, precision and rappel
    cmd = "./"+dimsumToCRF.pathCRFsuite+" tag -m "+dimsumToCRF.model+" -qt "+dimsumToCRF.BIN+"CRFfile.crf"
    sys.stderr.write(subprocess.check_output(['bash','-c', cmd]))


if not dimsumToCRF.keepCRFfile :
    os.remove(dimsumToCRF.BIN + "CRFfile.crf")
    util.verbose("Delete intermediary file (crfsuite format)\n")

