#! /usr/bin/env python
# -*- coding:UTF-8 -*-

################################################################################
# 
# Copyright 2010-2014 Carlos Ramisch, Vitor De Araujo, Silvio Ricardo Cordeiro,
# Sandra Castellanos
# 
# transform.py is part of mwetoolkit
# 
# mwetoolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# mwetoolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with mwetoolkit.  If not, see <http://www.gnu.org/licenses/>.
# 
################################################################################
"""
    For more information, call the script with no parameter and read the
    usage instructions.
"""

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import collections

from libs import util
from libs import filetype
from libs.base.sentence import Sentence
from libs.base.word import Word

################################################################################
# GLOBALS

usage_string = """\
Usage: {progname} OPTIONS <candidates-file>
Extract combined frequencies for Matheus Westhelle

The <candidates-file> must be in one of the filetype
formats accepted by the `--from` switch.


OPTIONS may be:

--from <input-filetype-ext>
    Force conversion from given filetype extension.
    (By default, file type is automatically detected):
    {descriptions.input[ALL]}

--to <output-filetype-ext>
    Convert input to given filetype extension.
    (By default, keeps input in original format):
    {descriptions.output[ALL]}

{common_options}
"""
input_filetype_ext = None
output_filetype_ext = None


################################################################################

class TransformHandler(filetype.ChainedInputHandler):
    """For each entity in the file, run the given commands."""
    def before_file(self, fileobj, ctxinfo):
        if not self.chain:
            self.chain = self.make_printer(ctxinfo, output_filetype_ext)
            self.key2cand = collections.OrderedDict()
        self.chain.before_file(fileobj, ctxinfo)

    def finish(self, ctxinfo):
        for cand in self.key2cand.values():
            self.chain.handle(cand, ctxinfo)
        self.chain.flush(ctxinfo)
        self.chain.finish(ctxinfo)

    def handle_candidate(self, candidate, ctxinfo):
        key = tuple(self.word2key(w) for w in candidate)
        if key not in self.key2cand:
            self.key2cand[key] = candidate
        else:
            other_cand = self.key2cand[key]
            other_cand.freqs.merge_from(candidate.freqs)
            for w, other_w in zip(candidate, other_cand):
                other_w.freqs.merge_from(w.freqs)

    def word2key(self, word):
        r"""The information that we care about when merging values."""
        return (word.pos, word.surface)


################################################################################

def treat_options( opts, arg, n_arg, usage_string ) :
    """Callback function that handles the command line options of this script.
    @param opts The options parsed by getopts. Ignored.
    @param arg The argument list parsed by getopts.
    @param n_arg The number of arguments expected for this script.
    """
    global input_filetype_ext
    global output_filetype_ext

    ctxinfo = util.CmdlineContextInfo(None, opts)
    util.treat_options_simplest(opts, arg, n_arg, usage_string)

    for o, a in ctxinfo.iter(opts):
        if o == "--from":
            input_filetype_ext = a
        elif o == "--to":
            output_filetype_ext = a
        else:
            raise Exception("Bad arg " + o)

################################################################################
# MAIN SCRIPT

longopts = ["from=", "to="]
args = util.read_options("", longopts, treat_options, -1, usage_string)
filetype.parse(args, TransformHandler(), input_filetype_ext)
