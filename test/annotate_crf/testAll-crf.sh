#! /bin/bash
HERE="$(cd "$(dirname "$0")" && pwd)"

source "$HERE/../testlib.sh"


t_args_DESCR="Test MWE annotation with CRF"
t_args_parse "$@"

t_testdir_configure


########################################

annotate() {
    local args="$1"
    local corpus="$2"
    local name_output="$3"

    local txt_out="$t_OUTDIR/${name_output}.txt"
    local txt_ref="$t_REFDIR/${name_output}.txt"

    local corpus="$t_LOCAL_INPUT/${corpus}"

    t_run "$t_BIN/annotate_crf.py $args $corpus > $txt_out"
    t_compare_with_ref "${name_output}.txt"
}

t_testname "Annotate MWE without arguments"
t_run "$t_BIN/train_crf.py $t_LOCAL_INPUT/spmrl-sentid.dev.gold.dimsum"
annotate '' "spmrl-sentid.test.gold.dimsum" "tag0"

t_testname "Annotate MWE from model1"
annotate '-a $t_LOCAL_INPUT/result_AM_FR.quantise -c $t_LOCAL_INPUT/crfsuite -f $t_LOCAL_INPUT/listFeatures.txt -m $t_LOCAL_INPUT/CRF.model1' "spmrl-sentid.test.gold.dimsum" "tag1"

t_testname "Annotate MWE with AM (french)"
annotate '-a $t_LOCAL_INPUT/result_AM_FR.quantise -f $t_LOCAL_INPUT/listFeatures_AM.txt -m $t_LOCAL_INPUT/CRF.model.AM -ev' "spmrl-sentid.test.gold.dimsum" "tag_AM"

