#! /usr/bin/env python

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

__all__ = [
    "parser", "Main"
]

import argparse
import collections
import itertools
import io
import json
import os
import re
import sys
from textwrap import dedent

FILE_ENC = "UTF-8"
HERE = os.path.dirname(os.path.realpath(__file__))
TOOLPATH = os.path.abspath(os.path.join(HERE, "..", "bin"))

HTDOCSPATH = os.path.abspath(os.path.join(HERE, "..", "docs", "htdocs"))
HTDOCSFTPATH = os.path.abspath(os.path.join(HTDOCSPATH, "MWE_070_File_types"))


RE_PARAGRAPH = re.compile(r"(\n *)+\n(?! )")

COMMANDS = {}

def make_command(func):
    COMMANDS[func.__name__] = func
    return func


def print_json(obj):
    json.dump(obj, sys.stdout, indent=3)
    print()


###########################################################

def html_escape(string):
    r"""Return HTML-escaped version of string."""
    import cgi
    return cgi.escape(string or "").replace("\n", "<br>")


def get_usage(toolpath):
    r"""Run `toolname` with `-h` and return output."""
    assert os.path.basename(toolpath) != "c-indexer", "TODO: add -h to c-indexer"
    from subprocess import Popen, PIPE
    if not os.path.exists(toolpath):
        raise Exception("Invalid toolpath: " + repr(toolpath))
    # XXX make this escape-friendly...
    p = Popen(toolpath + " -h",
            shell=True, stderr=PIPE, stdout=PIPE)
    return "".join(p.communicate())

def usage_paragraphs(text):
    r"""Split text and return a list of paragraphs."""
    p = RE_PARAGRAPH.split(text)
    p = [line.strip() for line in p]
    return [line for line in p if line]


def calculate_filetype2docspath():
    r"""Return a map {filetype -> HTDOCSFTPATH/XXX/body.inc}"""
    RE_STRINGNAME = re.compile(r'class="stringname"[^>]*>"(?P<thename>.*?)"')
    ret = {}
    for dirname in os.listdir(HTDOCSFTPATH):
        path = os.path.join(HTDOCSFTPATH, dirname, "body.inc")
        if os.path.isfile(path):
            with io.open(path, "r") as f:
                string_names = list(RE_STRINGNAME.finditer(f.read()))
                if len(string_names) >= 2:
                    raise Exception("Buggy HTML, go fix it: " + path)
                if string_names:
                    ret[string_names[0].group("thename")] = path
    return ret


def path2phite(path):
    r"""Return PHITE string for given path name."""
    phite_list = [subpath for subpath \
            in path.split("htdocs/")[1].split("/") \
            if subpath.startswith("MWE_")]
    ret = "?sitesig=MWE"
    if len(phite_list) >= 1:
        ret += "&page=" + phite_list[0]
    if len(phite_list) >= 2:
        ret += "&subpage=" + phite_list[1]
    return ret


############################################################

def toolpaths():
    r"""Return list with all tools in the toolkit."""
    for fname in sorted(os.listdir(TOOLPATH)):
        fullpath = os.path.join(TOOLPATH, fname)
        if os.path.isfile(fullpath) and os.stat(fullpath).st_mode & os.X_OK:
            yield fullpath

@make_command
def list_tools():
    r"""List all tools in the toolkit."""
    output = []
    for fname in os.listdir("."):
        if os.path.isfile(fname) and os.stat(fname).st_mode & os.X_OK:
            output.append(fname)
    print_json({"toolnames": output})


############################################################

@make_command
def tool_args(toolname):
    r"""List arguments for a tool."""
    toolargs = ParsedToolArgs()
    toolargs.parse(usage_paragraphs(get_usage(toolname)))
    toolargs.print_json()



############################################################


class AbstractArg(object):
    r"""Represents a tool argument with its associated info."""
    def arg_id(self):
        r"""Returns an ID that uniquely identifies this argument."""
        return self.miniflag or self.flag

    def to_html_block(self):
        r"""Return an HTML representing this argument (for blocks)."""
        raise NotImplementedError

    def to_html_header(self, anchor_prefix):
        r"""Return an HTML representing this argument (for header)."""
        raise NotImplementedError

    def to_dict(self):
        r"""Return a dict representing this argument."""
        raise NotImplementedError


class FlaggyArg(AbstractArg):
    r"""Parsed block with `--flag-name` + `<arg>` + `human description`."""
    def __init__(self, flag, miniflag, flag_args, human_descr):
        assert flag or miniflag, "One of {flag,miniflag} must be non-empty!"
        assert all(isinstance(arg, (PureArg, UnknownArg)) for arg in flag_args), flag_args
        self.flag, self.miniflag, self.flag_args, self.human_descr \
                = flag, miniflag, flag_args, dedent(human_descr).strip("\n")

    def update(self, other):
        r"""Update fields based on another FlaggyArg."""
        self.flag = self.flag or other.flag
        self.miniflag = self.miniflag or other.miniflag
        self.flag_args = self.flag_args or other.flag_args
        self.human_descr = "\n\n".join(
                filter(None, [self.human_descr, other.human_descr]))

    def arg_id(self):
        return self.flag or self.miniflag

    def to_html_block(self):
        flags = [html_escape(x) for x in [self.miniflag, self.flag] if x]
        if self.flag_args:
            joined_args = "".join(a.to_html_block() for a in self.flag_args)
            flags = [(x + " " + joined_args) for x in flags]
        return " OR ".join(flags)

    def to_html_header(self, anchor_prefix):
        flag = html_escape(self.miniflag or self.flag)
        joined_args = "".join(a.to_html_header(False) for a in self.flag_args)
        ret = flag + " " + joined_args
        if anchor_prefix:
            ret = r'<a class="header_blockref" ' \
                    r'href="#{prefix}/{id}">{ret}</a>'.format(
                    prefix=anchor_prefix, ret=ret, id=self.arg_id())
        return ret

    def to_dict(self):
        return collections.OrderedDict([
            ("miniflag", self.miniflag),
            ("flag", self.flag),
            ("flag_args", [a.to_dict() for a in self.flag_args]),
            ("human_descr", self.human_descr),
        ])


class PureArg(AbstractArg):
    r"""Parsed block with `<arg>`, or part of a FlaggyArg.
    A non-local `human description` can be added later.
    """
    def __init__(self, argument_name, description):
        self.argument_name, self.description \
                = argument_name, description
        self.human_descr = None

    def arg_id(self):
        return self.argument_name.replace("<", "@").replace(">", "@")

    def to_html_block(self):
        return html_escape(self.argument_name)

    def to_html_header(self, anchor_prefix):
        ret = html_escape(self.argument_name)
        if anchor_prefix:
            ret = r'<a class="header_blockref" href="#{prefix}/{id}">{ret}</a>'.format(
                    prefix=anchor_prefix, ret=ret, id=self.arg_id())
        return ret

    def to_dict(self):
        return collections.OrderedDict([
            ("arg_name", self.argument_name),
            ("description", self.description),
            ("human_descr", self.human_descr),
        ])


class ChoiceArg(AbstractArg):
    r"""An argument [--that | --is | -a <choice>].
    A non-local `human description` can be added later.
    """
    def __init__(self, choices):
        assert all(isinstance(c, AbstractArg) for c in choices)
        self.choices = choices
        self.human_descr = None

    def to_html_block(self):
        return "[" + " | ".join(ch.to_html_block() for ch in self.choices) + "]"

    def to_html_header(self, anchor_prefix):
        return "[" + " | ".join(ch.to_html_header(anchor_prefix) for ch in self.choices) + "]"

    def to_dict(self):
        return collections.OrderedDict([
            ("choices", [ch.to_dict() for ch in self.choices]),
            ("human_descr", self.human_descr),
        ])


class UnknownArg(AbstractArg):
    r"""A chunk of argument that we do not know how to parse."""
    def __init__(self, unknown_argtext):
        self.unknown_argtext = unknown_argtext

    def to_html_header(self, anchor_prefix):
        return html_escape(self.unknown_argtext)

    def to_html_block(self):
        return html_escape(self.unknown_argtext)

    def to_dict(self):
        return collections.OrderedDict([
            ("unknown_argtext", self.unknown_argtext),
        ])



class ParsedToolArgs(object):
    def __init__(self):
        self.toolname = None
        self.toolargs = None
        self.header_usage = None
        self.header_shortdescr = None

        # Whether we're still handling the
        # `required` section of the help message
        self.parsing_required = True

        # Map flagname -> FlaggyArg  (in the order seen in header)
        self.req_flags = collections.OrderedDict()
        # Map argument_name -> PureArg  (in the order seen in header)
        self.req_arguments = collections.OrderedDict()

        # List of other paragraphs with more detailed description
        self.header_extradescrs = []

        # Required section (first part) of the help message
        self.required = []

        # Optional section (second part) of the help message
        self.optional = []


    def parse(self, paragraphs):
        r"""Parse list of paragraphs and return JSON object."""
        for paragraph in paragraphs:
            self.current_p = paragraph
            if self.parsing_required:
                if paragraph.startswith("OPTIONS may"):
                    self.parsing_required = False
                    continue
                #DEPRECATED:
                #if paragraph == "Usage:":
                #    continue  # (found `Usage:` in a single line)
                if self.try_header():
                    continue
                if paragraph.startswith("-"):
                    arg = self.parse_paragraph_arg(paragraph)
                    self.update_req_flags(arg)
                    continue
                if self.try_input_human():
                    continue
                self.header_extradescrs.append(paragraph)
            else:
                if paragraph.startswith("-"):
                    arg = self.parse_paragraph_arg(paragraph)
                    #if arg.miniflag in self.req_flags:
                    #    print("WARNING: 
                    self.optional.append(arg)
                    continue
                self.header_extradescrs.append(paragraph)

        for obj in itertools.chain(self.required, self.optional):
            try:
                parse_human_descr(obj)
            except Exception:
                print(obj)
                raise


    def print_json(self):
        r"""Print arguments in STDOUT as a JSON."""
        print_json(collections.OrderedDict([
                ("toolname", self.toolname),
                #("toolargs", self.toolargs),
                #("usage", self.header_usage),
                ("shortdescr", self.header_shortdescr),
                ("extradescrs", self.header_extradescrs),
                ("required", [r.to_dict() for r in self.required]),
                ("optional", [o.to_dict() for o in self.optional])
        ]))


    def update_req_flags(self, arg):
        r"""Update `self.req_flags` with `arg`.
        Adds to `self.required` if needed.
        """
        try:
            self.req_flags[arg.flag].update(arg)
        except KeyError:
            try:
                self.req_flags[arg.miniflag].update(arg)
            except KeyError:
                print("WARNING: mandatory flag is not mentioned in header: " \
                        "`{flag}`".format(flag=arg.miniflag or arg.flag),
                        file=sys.stderr)
                self.required.append(arg)

    def update_optional_flags(self, arg):
        r"""Add arg to `self.optional`."""
        if arg.flag in self.req_flags or self.miniflag in self.req_flags:
            print("WARNING: optional flag is mandatory in header: " \
                    "`{flag}`".format(flag=arg.miniflag or arg.flag),
                    file=sys.stderr)
        arg.optional.append(arg)



    # TODO remove `python` as a possibility and required `Usage:`
    RE_HEADER = re.compile(r"[Uu]sage: *(?P<toolname>\S+) *" \
            "(?P<mandatory1>.*?) *\[?OPTIONS\]? *(?P<mandatory2>.*)")

    def try_header(self):
        r"""Interpret command header, as in `self.RE_HEADER`."""
        m = self.RE_HEADER.match(self.current_p)
        if not m: return False

        self.toolname = m.group("toolname")
        self.toolargs = (m.group("mandatory1") + " " +
                m.group("mandatory2")).strip()
        usage_and_descr = self.current_p.split("\n", 1)
        self.header_usage = usage_and_descr[0]
        if len(usage_and_descr) > 1:
            self.header_shortdescr = usage_and_descr[1]

        self.required.extend(self.parse_args(m.group("mandatory1")))
        self.required.extend(self.parse_args(m.group("mandatory2")))
        return True



    RE_INPUT_HUMAN = re.compile(r"The (?P<inputname><\S+>).* must be.*")

    def try_input_human(self):
        r"""Interpret paragraph "The <BLABLA> must be..."."""
        m = self.RE_INPUT_HUMAN.match(self.current_p)
        if not m: return False
        self.req_arguments[m.group("inputname")].human_descr = m.string
        return True


    RE_PARAGRAPH_HEADER_ARG = re.compile(
            "((?P<miniflag>-\S+)(?P<miniflag_arg> <\S+>)? (OR|or) )?" \
            "(?P<flag>--?\S+)(?P<flag_arg> <\S+>)?")

    def parse_paragraph_arg(self, paragraph):
        r"""Interpret paragraph starting with: "-f <arg> OR --flag <arg>"."""
        head, tail = paragraph.split("\n", 1)
        head = head.strip()  # Remove trailing spaces (so common...)
        m = self.RE_PARAGRAPH_HEADER_ARG.match(head)
        if m.end() != len(head):
            print("WARNING: Bad flag in tool:", head, file=sys.stderr)
        return FlaggyArg(m.group("flag"), m.group("miniflag"),
                self.parse_args(m.group("flag_arg") or ""), tail)



    RE_HEADER_ARG = re.compile(
            r"\[(?P<choice>.*?)\]" \
            "|(?P<flag>--?\S+?)(?P<flag_arg> <\S+?>(:<\S+?>)*)?" \
            "|(?P<argument_name><\S+?>(:<\S+?>)*)" \
            "|(?P<unknown>\S+?)")


    def parse_args(self, arg_string):
        r"""Parse a line such as: "[--flag <arg> | -g] --another-flag"."""
        ret = []
        for arg in self.RE_HEADER_ARG.finditer(arg_string):
            D = arg.groupdict()
            if D["choice"]:
                ret.append(ChoiceArg([self.parse_args(a)[0]
                        for a in D["choice"].split("|")]))

            elif D["flag"]:
                flag, miniflag = None, D["flag"]
                if miniflag.startswith("--"):
                    flag, miniflag = miniflag, flag
                ret.append(FlaggyArg(flag, miniflag,
                        self.parse_args(D["flag_arg"] or ""), ""))

                if self.parsing_required:
                    self.req_flags[D["flag"]] = ret[-1]

            elif D["argument_name"]:
                a = D["argument_name"]
                descr = argument_description(a)
                ret.append(PureArg(a, descr))

                if self.parsing_required:
                    self.req_arguments[a] = ret[-1]

            else:
                ret.append(UnknownArg(D["unknown"]))
        return ret



def argument_description(argument):
    r"""Parse an argument such as "<pattern-list>" or "<min>"."""
    if ">:<" in argument:
        a, b = argument.split(":", 1)
        return {"argument_type": "range", "components":
                [argument_description(a) for a in argument.split(":")],
                "separator": ":"}
    elif "pattern" in argument:
        return {"argument_type": "file", "category": "patterns"}
    elif "corpus" in argument:
        return {"argument_type": "file", "category": "corpus"}
    elif "candidate" in argument:
        return {"argument_type": "file", "category": "candidates"}
    elif "dict" in argument:
        return {"argument_type": "file", "category": "dict"}
    elif "input-file" in argument:
        return {"argument_type": "file", "category": "*any*"}
    elif "min" in argument or "max" in argument:
        return {"argument_type": "int", "argument_name": argument}
    else:
        return {"argument_type": "unknown"}



################# Post-processing #########################

RE_HUMAN_CHOICE = re.compile('^\* .*?"([^"]*)"', re.MULTILINE)
RE_HUMAN_FT_SWITCHNAME = re.compile(r"filetype formats accepted by the " \
        "`(?P<filetype_switchname>.*?)` switch".replace(" ", r"\s+"), re.DOTALL)

def parse_human_descr(ddescr):
    r"""Read `human_descr` field of `ddescr` and:
    
    1) Improve its data if finding descriptions of choice,
    such as the choice "Foo" in: '* "Foo": Bar bar baz'.

    2) Interpret `human_descr` field when it has the
    paragraph "...by the `BLABLA` switch...".
    """
    return  # TODO FIX THIS CODE
    human = ddescr.human_descr

    # 1)
    if ddescr.flag_args:
        D = ddescr.flag_args[0]["description"]
        if D["argument_type"] == "unknown":
            choice_strings = RE_HUMAN_CHOICE.findall(human)
            if choice_strings:
                D["argument_type"] = "choice_string"
                D["choice_strings"] = RE_HUMAN_CHOICE.findall(human)

    # 2)
    m = RE_HUMAN_FT_SWITCHNAME.search(human)
    if m is not None:
        ddescr["filetype_switchname"] = m.group("filetype_switchname")



###########################################################

parser = argparse.ArgumentParser(add_help=False, description="""
        Output information about the tools in the MWETOOLKIT.

        This information is captured by running each tool with
        the `-h` flag and parsing the output.

        This data can be used e.g. by graphical interfaces, when
        presenting the functionality of the toolkit in a higher-level
        user interface.  Each command-line option described in a tool's
        output is represented as a sub-object in the JSON output.
        """)
parser.add_argument("-h", "--help", action="help",
        help=argparse.SUPPRESS)
parser.add_argument("COMMAND", choices=tuple(COMMANDS.keys()),
        help="""Run this command (choices: {choices})""".format(
            choices=",".join(COMMANDS.keys())))
parser.add_argument("ARGS", nargs='*',
        help="""Pass on these arguments to `COMMAND`""")


class Main(object):
    def __init__(self, args):
        self.args = args

    def run(self):
        COMMANDS[self.args.COMMAND](*self.args.ARGS)


#####################################################

if __name__ == "__main__":
    Main(parser.parse_args()).run()
