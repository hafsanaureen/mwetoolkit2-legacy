<h2>TextualPattern file type</h2>

<!-- Short description -->
<p>
The "TextualPattern" file type represents a list of regex-style patterns
which can be matched against input corpora.
</p>

<p>
NOTE: This filetype is <strong>work in progress</strong>, and <em>will change</em> in the
near future.
</p>



<h3>Characteristics</h3>

<table border="1">
<tr><td><strong>String name<sup><a href="#fn1" id="ref1">1</a></sup></strong></td><td class="stringname">"TextualPattern"</td></tr>
<tr><td><strong>Categories</strong></td><td>"patterns" (input/output)</td></tr>
<tr><td><strong>Encoding</strong></td><td>Text file, UTF-8</td></tr>
<tr><td><strong>Source</strong></td><td>Designed for the mwetoolkit</td></tr>
<tr><td><strong>Automatic detection</strong></td><td>No (maybe in the future)</td></tr>
</table>



<h3>Details</h3>

The "TextualPattern" file type is a text file of category "patterns",
encoded in UTF-8. Each line should contain either a comment or a pattern,
which follows these (simplified) production rules:

<style>
.strlit_unquot, .strlit {
    color:#D00000;
}
.strlit:before { content: '"'; }
.strlit:after { content: '"'; }

.attribs {
    color:purple;
}

.strlit_emph {
    color:#A00000;
}
</style>

<ul>
  <li>S → SeqElems </li>
  <li>SeqElems → (Word | Seq | Either) (<span class="strlit"> </span> SeqElems)?</li>
</ul>
<ul>
  <li>Seq → <span class="strlit">seq(</span>
            SeqElems <span class="strlit">)</span>  </li>
</ul>

<ul>
  <li>Either → <span class="strlit">either(</span>
            EitherElems <span class="strlit">)</span> </li>
  <li>EitherElems → (Word | Seq) (<span class="strlit">|</span> EitherElems)?</li>
</ul>


The TextualPattern format defines two very simple Word representations:
<ul>
  <li>A sequence of lowercase characters matches a lemma (e.g. the TextualPattern <span class="strlit">walk</span>). </li>
  <li>A sequence of uppercase characters matches a POS-tag (e.g. the TextualPattern <span class="strlit">VPP</span>). </li>
</ul>

For more complex multilevel matchings, the following
Word representation can be used:
<ul>
  <li>Word → <span class="strlit">[</span>
            WordProps <span class="strlit">]</span>  </li>
  <li>WordProps → WordProp (<span class="strlit"> </span> WordProps)?</li>
  <li>WordProp → LiteralWordProp | RegexWordProp</li>
  <li>LiteralWordProp → PropName ("=" | "!=") String
        &nbsp; (e.g. <span class="strlit_unquot">lemma!=<span class="strlit_emph">"walk"</span></span>). </li>
  <li>RegexWordProp → PropName ("~" | "!~") Regex
        &nbsp; (e.g. <span class="strlit_unquot">surface~<span class="strlit_emph">/(up|down)grade[sd]?/</span></span>). </li>
</ul>


</p>
Any occurrence of Seq, Either or Word can be immediately followed by
a space-separated <span class="attribs">list of attributes</span>,
as defined in the
page <a href="?sitesig=MWE&page=MWE_020_Tutorials&subpage=MWE_030_Defining_Patterns">Defining XML Patterns</a>. For example:

<ul>
<li> <span class="strlit_unquot">either(être | avoir)</span><span class="attribs">{id=auxiliar repeat=?}</span>
<li> <span class="strlit_unquot">[pos~<span class="strlit_emph">/N.*/</span> lemma=<span class="strlit_emph">"thing"</span>]</span><span class="attribs">{repeat=+ id=noun_compound}</span>
</ul>




<h3>Escape table</h3>

</p>Inside a RegexWordProp, you must escape special characters such as "*" and "?", as determined by the <a href="https://docs.python.org/2/library/re.html">Python regex rules</a>.

<p>Elsewhere, special characters must be escaped as determined by this table:</p>

<p/>
<table border="1">
 <thead>
    <tr>
      <th>Unescaped</th>
      <th>Escaped</th>
      <th>Reason for escaping</th>
    </tr>
  </thead>
  <tbody>

    <tr>
      <td>"$"</td>
      <td>"${dollar}"</td>
      <td>This is the escaping character itself.</td>
    </tr>

    <tr>
      <td>"{"</td>
      <td>"${openbrace}"</td>
      <td>Used when defining a list of attributes and inside regexes.</td>
    </tr>

    <tr>
      <td>"}"</td>
      <td>"${closebrace}"</td>
      <td>Used when defining a list of attributes and inside regexes.</td>
    </tr>

    <tr>
      <td>"["</td>
      <td>"${openbracket}"</td>
      <td>Used when defining a Word and inside regexes.</td>
    </tr>

    <tr>
      <td>"]"</td>
      <td>"${closebracket}"</td>
      <td>Used when defining a Word and inside regexes.</td>
    </tr>

    <tr>
      <td>"("</td>
      <td>"${openparen}"</td>
      <td>Used when defining a Seq or Either.</td>
    </tr>

    <tr>
      <td>")"</td>
      <td>"${closeparen}"</td>
      <td>Used when defining a Seq or Either.</td>
    </tr>

    <tr>
      <td>"\""</td>
      <td>"${quot}"</td>
      <td>Separates literal prop values.</td>
    </tr>

    <tr>
      <td>"/"</td>
      <td>"${slash}"</td>
      <td>Separates regex prop values.</td>
    </tr>

    <tr>
      <td>"|"</td>
      <td>"${pipe}"</td>
      <td>Separates elements inside an Either.</td>
    </tr>

    <tr>
      <td>"="</td>
      <td>"${eq}"</td>
      <td>Used when defining a literal WordProp.</td>
    </tr>

    <tr>
      <td>"~"</td>
      <td>"${tilde}"</td>
      <td>Used when defining a regex WordProp.</td>
    </tr>

    <tr>
      <td>"!"</td>
      <td>"${bang}"</td>
      <td>Negates the value of a WordProp</td>
    </tr>

    <tr>
      <td>" "</td>
      <td>"${space}"</td>
      <td>Separates SeqElems, EitherElems and WordProps.</td>
    </tr>

    <tr>
      <td>"\n"</td>
      <td>"${newline}"</td>
      <td>Separates patterns (one per line).</td>
    </tr>

    <tr>
      <td>"#"</td>
      <td>"${hash}"</td>
      <td>Used as a comment prefix.</td>
    </tr>

    <tr>
      <td>"_"</td>
      <td>"${underscore}"</td>
      <td>[Undocumented]</td>
    </tr>

    <tr>
      <td>":"</td>
      <td>"${colon}"</td>
      <td>[Undocumented]</td>
    </tr>

    <tr>
      <td>"&amp;"</td>
      <td>"${amp}"</td>
      <td>[Undocumented]</td>
    </tr>

    <tr>
      <td>"\t"</td>
      <td>"${tab}"</td>
      <td>[Undocumented]</td>
    </tr>

   </tbody>
  </thead>
</table>

<p>Any output generated by the <code>mwetoolkit</code> will have these characters escaped as well.</p>



<h3>Code comments</h3>

<p>You can add a comment by prefixing any line with the special character "#".</p>




<h3>Example</h3>

<p>You can have a look at a sample <a
href="https://gitlab.com/mwetoolkit/mwetoolkit/tree/master/test/filetype-samples/patterns.TextualPattern">patterns</a>
file.</p>

<hr/>



<sup id="fn1">1. Case-sensitive string to be used with the <tt>filetype</tt>
directive and with script options such as <tt>--from</tt> and <tt>--to</tt>.<a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>
