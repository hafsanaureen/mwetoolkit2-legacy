<h2>Learning a CRF tagger</h2>

<p>If you want to identify MWEs in a corpus, you can now train a model and tag your corpus thanks to a CRF, using <a href="http://www.chokkan.org/software/crfsuite/" target="_blank">CRFsuite</a>.</p>

<p><strong>CRF</strong> stands for <a href="https://en.wikipedia.org/wiki/Conditional_random_field" target="_blank">conditional random fields</a>. 
It is a statistical supervised learning method very popular in natural language processing for sequence prediction tasks like part-of-speech tagging and named entity recognition.</p>

<h3>Training a MWE tagger</h3>

<p>We have implemented a tagger that uses <a href="https://en.wikipedia.org/wiki/Inside_Outside_Beginning" target="_blank">IOB encoding</a> to segment the MWEs in a text. This means that, instead of <a href="?sitesig=MWE&page=MWE_020_Tutorials&subpage=MWE_060_Annotating_a_corpus">annotating a corpus using a lexicon</a>, you need a corpus (manually) annotated with MWEs from which the tagger will be learned. This is the <em>training corpus</em>.</p>

<p>Currently, the toolkit only accepts the <a href="http://dimsum16.github.io/" target="_blank">dimsum format</a>, except that you do not need to provide supersenses. In addition to surface forms, the training corpus can also contain manual or automatic POS tags and lemmas. This information will help learning a better tagger for MWEs.</p>

<p>For instance, if you want to learn a CRF tagger using the DIMSUM training set, you can run the command below:</p>

<pre>
# Training your model on a giving corpus:
bin/train_crf.py test/CRF/input/dimsum16.train
</pre>

<p>The model is stored in a file called <code>test/CRF/CRF.model</code>.</p>

<p>You can personalize many options to train your CRF model. By default, the training script uses a list of features that you can find in <code>test/CRF/listFeatures.txt</code>. These are inspired from several articles like <a href="http://aclweb.org/anthology/Q14-1016">this one</a>, <a href="http://www.aclweb.org/anthology/W11-0809">this one</a> and <a href="https://aclweb.org/anthology/W/W16/W16-1816.pdf">this one</a>. However, you can specify your own list of features, using the option <code>--pathListFeatures</code>. These features must be in a textual file containing feature patterns, one per line. A pattern must respect the syntax below:</p>
<ul>
	<li><u>Orthographics features:</u></li>
	<ul>
		<li><code><strong>digits</strong></code> → Binary feature, 1 if the surface form contains digts, else 0.</li>
		<li><code><strong>hyphen</strong></code> → Binary feature, 1 if the surface form contains a hyphen, else 0.</li>
		<li><code><strong>capitalized</strong></code> → Binary feature, 1 if the surface form starts with an uppercase letter, else 0.</li>
		<li><code><strong>allCapitalized</strong></code> → Binary feature, 1 if the surface form contains only by uppercase letters, else 0.</li>
		<li><code><strong>capitalizedAndBOS</strong></code> → Binary feature, 1 if the surface form starts with an uppercase letter AND is at the beginning of a sentence, else 0.</li>
	</ul>
	<li>N-gram features:</li>
	<ul>
	  <li><code><strong>[wlp]\[\-?[0-9]\](\|[wlp]\[\-?[0-9]\])*</strong></code>: This regular expression describes sequences of pipe-separated (<code>|</code>) word surface forms (<code>w</code>), lemmas (<code>l</code>) or parts of speech (<code>p</code>) and their relative position with respect to the current token in squared brackets (<code>[i]</code>). Indeed, it is not very pleasant to read, so here are some examples: </li>
	  <ul> 
		  <li><code><strong>p[-2]</strong></code> → Part of speech of the token located 2 words before the current  one.</li>
		  <li><code><strong>l[-1]|l[0]|l[1]</strong></code> → Trigram composed of the lemma of the previous, current and next tokens.</li>
		  <li><code><strong>w[-1]|l[9]|w[0]|p[3]</strong></code> → Not very useful, but this 4-gram is a valid n-gram feature.</li>
	  </ul>
	</ul>
	<li>Lexical features</li>
	  <ul>
		<li><strong><code>AM_.*</code></strong> → Any feature prefixed with <code>AM_</code> is accepted if you provide a file with lexical features (see below). For example, if you define a feature named <code>AM_mle</code>, then your lexical features file must have a column named <code>mle</code>.
		</ul>
	</li>
</ul>

<h3>Lexical features</h3>

<p>Sometimes, you want to add external resources to guide tagging decisions. This is typically the case when you have available, handcrafted or automatically build MWE dictionaries. As shown in <a href="">scientific papers</a>, these can greatly enhance the quality of MWE identification.</p>

<p>In order to indicate to the tagger that you want to use lexical features, you must use the option <code>--pathFileAM</code>. AM actually stands for <em>association measures</em>, which are a type of lexical features. We will rename this in the future.</p>

<p>This file, in TSV format (tab-separated values), must contain a column named <code>ngram</code> with the lemmas of the matching lexical units. Then, you can add columns corresponding to the values of lexical features, like <code>1</code> if you just want a binary on/off feature. If the lexical features are automatically extracted, please remember that you should quantise all real values, otherwise CRFSuite will not know how to handle them.</p>

<p>The easiest way to understand lexical features is probably by checking the example file, available in <code>test/CRF/result_AM_EN.quantise</code></p>

<h3>Annotating/tagging a corpus</h3>

<p>Now, you have trained your model! You can now annotate your test corpus. For instance, if you want to annotate <code>dimsum16.test</code> file, you can run:</p>

<pre>
# Annotate your corpus
bin/annotate_crf.py test/CRF/input/dimsum16.test > tags.txt
</pre>

<p>The list of predicted IOB tags will be stored in a file named <code>tag.txt</code>. Unfortunately, we do not yet output other formats for the MWE tags, but this is work in progress. You will need to paste the tags file with your input corpus to rebuild the result, you can do this using the <code>paste</code> command on the terminal.</p>


<p>By default, the script looks for the model file <code>test/CRF/CRF.model</code>. If you want to use another model, use <code>--model</code> </p>

<p>If you ever have the gold tags in your file, you can use the option <code>--eval</code> to evaluate the performance of the model.</p>



