# Commands for the MWETOOLKIT tutorial


###############################################################################
# 1) Run the installation

cd mwetoolkit
make
cd ..


###############################################################################
# 2) Play with input corpus

# Run any script's help
mwetoolkit/bin/head.py -h

# Show first 2 sentences
mwetoolkit/bin/head.py -n 2 ted-en-small.conll

# Show and convert 2 first sentences into other formats
mwetoolkit/bin/head.py -n 2 --to PlainCorpus ted-en-small.conll
mwetoolkit/bin/head.py -n 2 --to Moses ted-en-small.conll

# Count number of sentences and words in corpus
mwetoolkit/bin/wc.py ted-en-small.conll

# Convert the input into XML format
mwetoolkit/bin/transform.py --to=XML ted-en-small.conll > ted-en-small.xml
head ted-en-small.xml


###############################################################################
# 3) Look for interesting words in the corpus

# Look for all sentences containing the word "show"
mwetoolkit/bin/grep.py -e "show" --to=HTML ted-en-small.xml > show.html
# Open show.html in your browser, for instance
firefox show.html

# Alternatively, use the command line viewer
mwetoolkit/bin/grep.py -e "(show|hide)" ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus
# Hint: type 'q' to quit the viewer

# Now suppose you're only interested in "show" as a noun
mwetoolkit/bin/grep.py -e "[lemma=show pos=NN1]" ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus


###############################################################################
# 5) Look for interesting word sequences in the corpus

# All sequences of two consecutive nouns
mwetoolkit/bin/grep.py -e "[pos=NN1] [pos=NN1]" ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus

# All sequences of 2 or more consecutive nouns
mwetoolkit/bin/grep.py -e "[pos=NN1] [pos=NN1]{repeat=+}" ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus

# Alternatively, use the XML pattern expressing slightly more sophisticated noun phrases
mwetoolkit/bin/grep.py -p pat_nn.xml ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus


###############################################################################
# 6) Look for interesting subtrees in the corpus

# All verbs and their indirect object nouns occurring to the right
mwetoolkit/bin/grep.py -e "[pos~/V.*/]{id=v1} []{repeat=* ignore=true} [pos=NN1 syndep=dobj:v1]" ted-en-small.xml | mwetoolkit/bin/view.py --to=PlainCorpus

# Go further: try to encode a more complex MWE like "give -obj-> [the green light]"


###############################################################################
# 7) Extract and filter MWE candidates to build a lexicon

# Generate a list of all noun compounds that match the pattern
mwetoolkit/bin/candidates.py -p pat_nn.xml ted-en-small.xml > cand-nn.xml

# Go further: how do you apply the regexp? Longest, shortest, all matches?

# Count the compound and the individual words in the corpus
mwetoolkit/bin/index.py --attributes=lemma:pos -i index/ted-small ted-en-small.conll
mwetoolkit/bin/counter.py -i index/ted-small.info cand-nn.xml > cand-nn-count.xml

# Calculate association measures for the candidates
mwetoolkit/bin/feat_association.py cand-nn-count.xml  > cand-nn-count-assoc.xml
# Hint: you can output CSV and open in spreadsheet editors like Calc or Excel
mwetoolkit/bin/feat_association.py --to=CSV cand-nn-count.xml > cand-nn-count-assoc.csv

# Use the association measures to filter the list, generate a final lexicon
mwetoolkit/bin/sort.py -d --feat t_ted-small cand-nn-count-assoc.xml | mwetoolkit/bin/head.py -n 20 > lexicon-nn.xml


###############################################################################
# 8) Annotate a corpus with the entries of a lexicon

# Annotate (tokenise) the complex prepositions in the corpus
mwetoolkit/bin/annotate_mwe.py --filter-and-annot -c complex-prep.txt --to=PlainCorpus ted-en-small.conll

# Annotate the noun compounds of previous step in the corpus
mwetoolkit/bin/annotate_mwe.py --filter-and-annot  -c lexicon-nn.xml --to=HTML ted-en-small.conll > annot-nn.html
firefox annot-nn.html

# Go further: What happens with overlapping expressions in HTML? and XML?
# Go further: I only want to annotate instances matched by the initial pattern


