<?php

$namechars['__apos__'] = "'";
$namechars['__question__'] = "?";
$namechars['__exclaim__'] = "!";
$namechars['__colon__'] = ":";
$namechars['__comma__'] = ",";
$namechars['__period__'] = ".";
$namechars['__lb__'] = "(";
$namechars['__rb__'] = ")";
$namechars['__ldots__'] = "&hellip;";
$namechars['__amp__'] = "&amp;";
$namechars['__quot__'] = "&quot;";

?>